﻿using System.Net.Http;

namespace SunProject.Shared.Helpers
{
    public interface IApiClientHelper
    {
        HttpClient ApiClient { get; set; }
    }
}