﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SunProject.Shared.Models
{
    public class SunData
    {
        public int Id { get; set; }

        [Display(Name = "City")]
        public City City { get; set; }
        
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        
        [Display(Name = "Sunrise")]
        public DateTime Sunrise { get; set; }
        
        [Display(Name = "Sunset")]
        public DateTime Sunset { get; set; }
    }
}
