﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SunProject.Shared.Models
{
    public class User
    {
        public int Id { get; set; }

        [Display(Name = "Username")]        
        public string Login { get; set; }
        
        public string Password { get; set; }

        [Display(Name = "Admin Access")]
        public bool? IsAdmin { get; set; }

    }
}
