﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Api.Helpers
{
    public class SunApiResults
    {
        public SunApiResultsInfo Results { get; set; }
    }
    public class SunApiResultsInfo
    {
        public DateTime Sunrise { get; set; }
        public DateTime Sunset { get; set; }
    }
}
