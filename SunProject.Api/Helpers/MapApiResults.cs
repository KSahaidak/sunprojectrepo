﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Api.Helpers
{
    // only necessary part of json results
    public class MapApiResults
    {
        public List<ResourceSet> resourceSets { get; set; }
        // if there is a gibberish city name - returns center of Ukraine
        public const double PointOfNowhereLat = 49.172939300537109;
        public const double PointOfNowhereLng = 31.266984939575195;
    }
    public class Point
    {
        public List<double> coordinates { get; set; } = new List<double>();
    }
    public class Resource
    {
        public Point point { get; set; }
    }
    public class ResourceSet
    {
        public List<Resource> resources { get; set; }
    }
}
