﻿using Microsoft.EntityFrameworkCore;
using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Api.DataContext
{
    public class SunInfoDataContext : DbContext
    {
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<SunData> SunDatas { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public SunInfoDataContext()
        {
            // for mocking purposes
        }
        public SunInfoDataContext(DbContextOptions<SunInfoDataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            User[] Users =
            {
                new User() { Id = 1, Login = "admin", Password = "admin", IsAdmin = true }
            };
            modelBuilder.Entity<User>().HasData(Users);

            City[] Cities =
            {
                new City(){ Id = 1, Name = "Запорожье", Latitude = 47.84645462036133 , Longitude = 35.149269104003906 },
                new City(){ Id = 2, Name = "Киев", Latitude = 50.447731018066406 , Longitude = 30.54271697998047 },
                new City(){ Id = 3, Name = "Одесса", Latitude = 46.47661209106445 , Longitude = 30.707307815551758 }
            };
            modelBuilder.Entity<City>().HasData(Cities);

            modelBuilder.Entity<SunData>()
                .HasOne(p => p.City)
                .WithMany(t => t.Suninfo)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
