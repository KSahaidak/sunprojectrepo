﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SunProject.Api.Helpers;
using SunProject.Api.Services;
using SunProject.Shared.Models;

namespace SunProject.Api.Controllers
{
    [ApiController]
    [Route("api/Cities")]
    public class CitiesController : ControllerBase
    {
        IDataProcessService data;
        IApiService<Point, string> api;

        public CitiesController(IDataProcessService datacontext, IApiService<Point, string> apicontext)
        {
            data = datacontext;
            api = apicontext;
        }
        // List
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<City> cities = await data.LoadCitiesList();
            return Ok(cities);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> FindCity(int id)
        {
            City city = await data.FindCity(id);
            if (city != null)
            {
                return Ok(city);
            }
            else
            {
                return Conflict("City not found");
            }
        }

        // Create
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] City city)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(city.Name))
                {
                    return Conflict("Invalid City Name format: cannot be empty.");
                }
                if (await data.CityExist(city.Name) == true)
                {
                    return Conflict("There is a City with this name on the list.");
                }

                Point location = await api.GetApiData(city.Name);

                if (location.coordinates[0] == MapApiResults.PointOfNowhereLat &&
                    location.coordinates[1] == MapApiResults.PointOfNowhereLng)
                {
                    return Conflict("Invalid City name.");
                }
                else
                {
                    City newcity = new City() { Name = city.Name, Latitude = location.coordinates[0], Longitude = location.coordinates[1] };
                    await data.AddCity(newcity);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Unable to register user: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // Edit
        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] City city)
        {
            try
            {
                if (await data.FindCity(city.Id) != null)
                {
                    if (string.IsNullOrWhiteSpace(city.Name))
                    {
                        return Conflict("Invalid City Name format: cannot be empty.");
                    }
                    await data.EditCity(city);
                    return Ok();
                }
                else
                {
                    return BadRequest("There is no city to change");
                }
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Unable to edit city: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await data.DeleteCity(id);
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Unable to delete city: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
