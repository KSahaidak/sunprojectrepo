﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SunProject.Api.Helpers;
using SunProject.Api.Services;
using SunProject.Shared.Models;

namespace SunProject.Api.Controllers
{
    [ApiController]
    [Route("api/Sun")]
    public class SunController : ControllerBase
    {
        IDataProcessService data;
        IApiService<SunApiResultsInfo, City> api;
        public SunController(IDataProcessService datacontext, IApiService<SunApiResultsInfo, City> apicontext)
        {
            data = datacontext;
            api = apicontext;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            await SyncSunInfo();
            List<SunData> sundata = await data.LoadSunData();
            return Ok(sundata);
        }

        public async Task SyncSunInfo()
        {
            // Getting only today's data
            // Comparing it to the list of added cities to see if some is missing 
            // (in case if city was added after initial synchronizing)
            // Adding missing data

            List<SunData> sundata = await data.LoadSunData();
            List<City> cities = await data.LoadCitiesList();
            var excludedIDs = sundata.Select(p => p.City.Id).ToList();
            if (excludedIDs != null)
            {
                var missingCities = cities.Where(c => !excludedIDs.Contains(c.Id));
                if (missingCities != null)
                {
                    foreach (City city in missingCities)
                    {
                        SunData currentcitydata = await AddSunInfo(city);
                        await data.AddSunData(currentcitydata);
                    }
                }
            }
        }
        // add sun info
        public async Task<SunData> AddSunInfo(City city)
        {
            SunApiResultsInfo info = await api.GetApiData(city);
            SunData data = new SunData()
            {
                City = city,
                Date = DateTime.Now.Date,
                Sunrise = info.Sunrise,
                Sunset = info.Sunset
            };
            return data;
        }
    }
}
