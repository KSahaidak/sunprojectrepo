﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SunProject.Api.Helpers;
using SunProject.Api.Services;
using SunProject.Shared.Models;

namespace SunProject.Api.Controllers
{
    [ApiController]
    [Route("api/Users")]
    public class UsersController : ControllerBase
    {
        IDataProcessService data;

        public UsersController(IDataProcessService datacontext)
        {
            data = datacontext;
        }

        // List
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<User> users = await data.LoadUsersList();
            return Ok(users);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> FindUser(int id)
        {
            try
            {
                User user = await data.FindUser(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] User loginuser)
        {
            try
            {
                User user = await data.FindUser(loginuser.Login, loginuser.Password);
                return Ok(user);
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Login error: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
        // Create
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]User user)
        {
            try
            {
                if (await data.UserExist(user.Login))
                {
                    return Conflict("Login is unavailable.");
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(user.Login))
                    {
                        return Conflict("Wrong login format. Login cannot be empty or a whitespace.");
                    }
                    if (string.IsNullOrWhiteSpace(user.Password))
                    {
                        return Conflict("Wrong password format. Password cannot be empty or a whitespace.");
                    }
                    user.IsAdmin = false;
                    await data.AddUser(user);
                    return Ok();
                }
            }
            catch(Exception ex)
            {
                Logger.Log.Error("Unable to register user: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // Edit
        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] User user)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(user.Password) && user.IsAdmin == null)
                {
                    return Conflict("No data to change.");
                }
                if (!string.IsNullOrEmpty(user.Password))
                {
                    if (string.IsNullOrWhiteSpace(user.Password))
                    {
                        return Conflict("Wrong password format. Password cannot be empty or a whitespace.");
                    }
                    await data.EditUserPassword(user);
                }
                if (user.IsAdmin != null)
                {
                    await data.EditUserAccess(user);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Unable to edit user: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await data.DeleteUser(id);
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Unable to delete user: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

    }
}
