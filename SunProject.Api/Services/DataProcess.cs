﻿using Microsoft.EntityFrameworkCore;
using SunProject.Api.DataContext;
using SunProject.Api.Helpers;
using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Api.Services
{
    public class DataProcess : IDataProcessService
    {
        private SunInfoDataContext db;
        public DataProcess(SunInfoDataContext context)
        {
            db = context;
        }

        // USER
        // find user
        public async Task<User> FindUser(int id)
        {
            User user = await db.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new ArgumentException("User not found", "id");
            }
        }
        public async Task<bool> UserExist(string login)
        {
            User user = await db.Users.FirstOrDefaultAsync(x => x.Login == login);
            if (user != null)
            {
                return true;
            }
            else return false;
        }
        public async Task<User> FindUser(string login, string password)
        {
            User user = await db.Users.SingleOrDefaultAsync(u => u.Login == login && u.Password == password);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new ArgumentException("Invalid login information, user not found");
            }
        }

        // load users list
        public async Task<List<User>> LoadUsersList()
        {
            List<User> users = await db.Users.ToListAsync();
            return users;
        }

        // add user
        public async Task AddUser(User user)
        {
            db.Users.Add(user);
            await db.SaveChangesAsync();
            Logger.Log.Info("User registered: " + user.Login);
        }

        // edit user
        public async Task EditUserPassword(User edituser)
        {
            User user = await FindUser(edituser.Id);
            user.Password = edituser.Password;
            await db.SaveChangesAsync();
        }
        public async Task EditUserAccess(User edituser)
        {
            User user = await FindUser(edituser.Id);
            user.IsAdmin = edituser.IsAdmin;
            await db.SaveChangesAsync();
            if (user.IsAdmin == true)
            {
                Logger.Log.Info("Admin rights granted to: " + user.Login);
            }
            else
            {
                Logger.Log.Info("Admin rights revoked from: " + user.Login);
            }
        }

        // delete user
        public async Task DeleteUser(int id)
        {
            User user = await FindUser(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            Logger.Log.Info("User deleted: " + user.Login);
        }

        // CITY
        // list 
        public async Task<List<City>> LoadCitiesList()
        {
            var cities = await db.Cities.ToListAsync();
            return cities;
        }

        //find
        public async Task<City> FindCity(int id)
        {
            City city = await db.Cities.FirstOrDefaultAsync(x => x.Id == id);
            if (city != null)
            {
                return city;
            }
            else
            {
                throw new ArgumentException("City not found");
            }
        }
        public async Task<bool> CityExist(string name)
        {
            City city = await db.Cities.FirstOrDefaultAsync(x => x.Name == name);
            if (city != null)
            {
                return true;
            }
            else return false;
        }

        // add
        public async Task AddCity(City city)
        {
            db.Cities.Add(city);
            await db.SaveChangesAsync();
            Logger.Log.Info("Added new City: " + city.Name);
        }

        // edit
        public async Task EditCity(City city)
        {
            City editcity = await FindCity(city.Id);
            editcity.Name = city.Name;
            editcity.Latitude = city.Latitude;
            editcity.Longitude = city.Longitude;
            await db.SaveChangesAsync();
            Logger.Log.Info("City was edited: " + editcity.Name);
        }

        //delete
        public async Task DeleteCity(int id)
        {
            City city = await FindCity(id);
            db.Cities.Remove(city);
            await db.SaveChangesAsync();
            Logger.Log.Info("Deleted City: " + city.Name);
        }


        // SUN 
        public async Task<List<SunData>> LoadSunData()
        {
            List<SunData> sundata = await db.SunDatas.Include(x => x.City)
                .Where(d => d.Date == DateTime.Now.Date)
                .ToListAsync();
            return sundata;
        }
        public async Task AddSunData(SunData data)
        {
            db.SunDatas.Add(data);
            await db.SaveChangesAsync();
            Logger.Log.Info("Sun data received");
        }
    }
}
