﻿using Newtonsoft.Json;
using SunProject.Api.Helpers;
using SunProject.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SunProject.Api.Services
{
    public class MapApiProcess : IApiService<Point, string>
    {
        IApiClientHelper api;
        public MapApiProcess(IApiClientHelper _api)
        {
            api = _api;
        }
        public async Task<Point> GetApiData(string city)
        {
            string url = $"http://dev.virtualearth.net/REST/v1/Locations/Ukraine/{ city }/-/-/-?o=json&inclnb=1&key=AnmvweojDKCPu2WiAMc3pMMIUCcNxSJfiEN6RMsDi5Ae7-K3-_8DUrKdLaBKEjvA";
            using (HttpResponseMessage responce = await api.ApiClient.GetAsync(url))
            {
                if (responce.IsSuccessStatusCode)
                {
                    string results = await responce.Content.ReadAsStringAsync();
                    MapApiResults root = JsonConvert.DeserializeObject<MapApiResults>(results);
                    Point point = root.resourceSets[0].resources[0].point;
                    return point;
                }
                else
                {
                    Logger.Log.Error("HttpResponce: " + responce.ReasonPhrase);
                    throw new Exception(responce.ReasonPhrase);
                }
            }
        }
    }
}
