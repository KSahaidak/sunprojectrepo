﻿using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Api.Services
{
    // database context access
    public interface IDataProcessService
    {
        // user
        Task<User> FindUser(int id);
        Task<bool> UserExist(string login);
        Task<User> FindUser(string login, string password);
        Task<List<User>> LoadUsersList();
        Task AddUser(User user);
        Task EditUserPassword(User user);
        Task EditUserAccess(User user);
        Task DeleteUser(int id);

        // city
        Task<List<City>> LoadCitiesList();
        Task<City> FindCity(int id);
        Task<bool> CityExist(string name);
        Task AddCity(City city);
        Task EditCity(City city);
        Task DeleteCity(int id);

        //sun info
        Task<List<SunData>> LoadSunData();
        Task AddSunData(SunData data);
    }
}
