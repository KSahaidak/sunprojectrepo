﻿using SunProject.Api.Helpers;
using SunProject.Shared.Helpers;
using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SunProject.Api.Services
{
    public class SunApiProcess : IApiService<SunApiResultsInfo, City>
    {
        IApiClientHelper api;
        public SunApiProcess(IApiClientHelper _api)
        {
            api = _api;
        }
        public async Task<SunApiResultsInfo> GetApiData(City city)
        {
            string url = $"https://api.sunrise-sunset.org/json?lat={ city.Latitude }&lng={ city.Longitude }&date=today";
            using (HttpResponseMessage responce = await api.ApiClient.GetAsync(url))
            {
                if (responce.IsSuccessStatusCode)
                {
                    var results = await responce.Content.ReadAsAsync<SunApiResults>();
                    return results.Results;
                }
                else
                {
                    Logger.Log.Error("HttpResponce: " + responce.ReasonPhrase);
                    throw new Exception(responce.ReasonPhrase);
                }
            }
        }
    }
}
