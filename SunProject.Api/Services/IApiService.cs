﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Api.Services
{
    public interface IApiService<T, K>
    {
        Task<T> GetApiData(K city);
    }
}
