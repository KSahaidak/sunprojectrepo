﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SunProject.Client.Models;
using SunProject.Shared.Helpers;
using SunProject.Shared.Models;

namespace SunProject.Client.Controllers
{
    public class CitiesController : Controller
    {
        IApiClientHelper api;
        public CitiesController(IApiClientHelper _api)
        {
            api = _api;
        }
        // List of cities
        public async Task<ActionResult> Index()
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.GetAsync(Startup.ApiBaseUrl + "Cities");
                string apiResponce = await responce.Content.ReadAsStringAsync();
                if (responce.IsSuccessStatusCode)
                {
                    List<City> cities = JsonConvert.DeserializeObject<List<City>>(apiResponce);
                    return View(cities);
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }

        // Add new city
        [Authorize(Roles = "IsAdmin")]
        public ActionResult Add()
        {
            return View();
        }
        [Authorize(Roles = "IsAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(CityViewModel model)
        {
            try
            {
                StringContent city = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                HttpResponseMessage responce = await api.ApiClient.PostAsync(Startup.ApiBaseUrl + "Cities/", city);
                if (responce.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    string apiResponce = await responce.Content.ReadAsStringAsync();
                    ModelState.AddModelError("", apiResponce.Trim('"'));
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
            return View();
        }

        // Edit City
        [Authorize(Roles = "IsAdmin")]
        public async Task<ActionResult> Edit(int id)
        {
            HttpResponseMessage responce = await api.ApiClient.GetAsync(Startup.ApiBaseUrl + "Cities/" + id);
            string apiResponce = await responce.Content.ReadAsStringAsync();
            if (responce.IsSuccessStatusCode)
            {
                City city = JsonConvert.DeserializeObject<City>(apiResponce);
                return View(city);
            }
            else
            {
                return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
            }
        }

        [Authorize(Roles = "IsAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(City model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    StringContent editcity = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await api.ApiClient.PutAsync(Startup.ApiBaseUrl + "Cities/", editcity);
                    if (responce.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        string apiResponce = await responce.Content.ReadAsStringAsync();
                        return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                    }
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
                }
            }
            return View();
        }

        // Delete City
        [Authorize(Roles = "IsAdmin")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.GetAsync(Startup.ApiBaseUrl + "Cities/" + id);
                string apiResponce = await responce.Content.ReadAsStringAsync();
                if (responce.IsSuccessStatusCode)
                {
                    City city = JsonConvert.DeserializeObject<City>(apiResponce);
                    return View(city);
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }

        [Authorize(Roles = "IsAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, IFormCollection collection)
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.DeleteAsync(Startup.ApiBaseUrl + "Cities/" + id);
                if (responce.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }
    }
}
