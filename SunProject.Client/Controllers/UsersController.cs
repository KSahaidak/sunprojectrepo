﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SunProject.Client.Helpers;
using SunProject.Client.Models;
using SunProject.Shared.Helpers;
using SunProject.Shared.Models;

namespace SunProject.Client.Controllers
{
    public class UsersController : Controller
    {
        IApiClientHelper api;
        public UsersController(IApiClientHelper _api)
        {
            api = _api;
        }
        // List of all users, only for admins 
        [Authorize(Roles = "IsAdmin")]
        public async Task<IActionResult> Index()
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.GetAsync(Startup.ApiBaseUrl + "Users/");
                string apiResponce = await responce.Content.ReadAsStringAsync();
                if (responce.IsSuccessStatusCode)
                {
                    List<User> users = JsonConvert.DeserializeObject<List<User>>(apiResponce);
                    return View(users);
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }                
        }

        //User Registration
        [HttpGet]
        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration(RegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                StringContent user = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                HttpResponseMessage responce = await api.ApiClient.PostAsync(Startup.ApiBaseUrl + "Users/", user);
                if (responce.IsSuccessStatusCode)
                {
                    return RedirectToAction("Login", "Users");
                }
                else
                {
                    string apiResponce = await responce.Content.ReadAsStringAsync();
                    ModelState.AddModelError("", apiResponce.Trim('"'));
                }
            }
            return View();
        }

        ////Log in to the system
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    StringContent user = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await api.ApiClient.PostAsync(Startup.ApiBaseUrl + "Users/Login/", user);
                    string apiResponce = responce.Content.ReadAsStringAsync().Result; 
                    if (responce.IsSuccessStatusCode)
                    {
                        UserData.CurrentUser = JsonConvert.DeserializeObject<User>(apiResponce);
                        if (UserData.CurrentUser.IsAdmin == true)
                        {
                            List<Claim> claims = new List<Claim>();
                            claims.Add(new Claim(ClaimTypes.Role, "IsAdmin"));
                            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", apiResponce.Trim('"'));
                    }
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
                }
            }
            return View();
        }

        // Logout
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            UserData.CurrentUser = null;
            return RedirectToAction("Login", "Users");
        }

        // Edit users - grant or revoke admin access
        [Authorize(Roles = "IsAdmin")]
        public async Task<IActionResult> GrantAccess(int id)
        {
            try
            {
                User user = new User() { Id = id, IsAdmin = true };
                StringContent edituser = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                HttpResponseMessage responce = await api.ApiClient.PutAsync(Startup.ApiBaseUrl + "Users/", edituser);
                if (responce.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    string apiResponce = await responce.Content.ReadAsStringAsync();
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }

        [Authorize(Roles = "IsAdmin")]
        public async Task<IActionResult> RevokeAccess(int id)
        {
            try
            {
                User user = new User() { Id = id, IsAdmin = false };
                StringContent edituser = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                HttpResponseMessage responce = await api.ApiClient.PutAsync(Startup.ApiBaseUrl + "Users/", edituser);
                if (responce.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    string apiResponce = await responce.Content.ReadAsStringAsync();
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }

        // Change password
        [HttpGet]
        public ActionResult ChangePassword()
        {
            if (UserData.CurrentUser != null)
            {
                RegistrationViewModel editUser = new RegistrationViewModel() { Login = UserData.CurrentUser.Login };
                return View(editUser);
            }
            else{
                return RedirectToAction("Error", "Home", new { exMessage = "You aren't logged in to the system" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(LoginViewModel model)
        {
            if (UserData.CurrentUser != null)
            {
                try
                {
                    UserData.CurrentUser.Password = model.Password;
                    User change = new User() { Id = UserData.CurrentUser.Id, Password = model.Password, IsAdmin = null };
                    StringContent edituser = new StringContent(JsonConvert.SerializeObject(change), Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await api.ApiClient.PutAsync(Startup.ApiBaseUrl + "Users/", edituser);
                    if (responce.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        string apiResponce = await responce.Content.ReadAsStringAsync();
                        return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                    }
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
                }
            }
            else
            {
                return RedirectToAction("Error", "Home", new { exMessage = "You aren't logged in to the system" });
            }
        }

        // Delete user
        [Authorize(Roles = "IsAdmin")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.GetAsync(Startup.ApiBaseUrl + "Users/" + id);
                string apiResponce = await responce.Content.ReadAsStringAsync();
                if (responce.IsSuccessStatusCode)
                {
                    User user = JsonConvert.DeserializeObject<User>(apiResponce);
                    return View(user);
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }

        [Authorize(Roles = "IsAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, IFormCollection collection)
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.DeleteAsync(Startup.ApiBaseUrl + "Users/" + id);
                if (responce.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }
    }
}
