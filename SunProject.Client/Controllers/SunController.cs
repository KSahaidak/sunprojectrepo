﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SunProject.Shared.Helpers;
using SunProject.Shared.Models;

namespace SunProject.Client.Controllers
{
    public class SunController : Controller
    {
        IApiClientHelper api;
        public SunController(IApiClientHelper _api)
        {
            api = _api;
        }
        public async Task<ActionResult> Index()
        {
            try
            {
                HttpResponseMessage responce = await api.ApiClient.GetAsync(Startup.ApiBaseUrl + "Sun");
                string apiResponce = await responce.Content.ReadAsStringAsync();
                if (responce.IsSuccessStatusCode)
                {
                    List<SunData> data = JsonConvert.DeserializeObject<List<SunData>>(apiResponce);
                    return View(data);
                }
                else
                {
                    return RedirectToAction("Error", "Home", new { exMessage = responce.ReasonPhrase + " " + apiResponce });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { exMessage = ex.Message });
            }
        }
    }
}
