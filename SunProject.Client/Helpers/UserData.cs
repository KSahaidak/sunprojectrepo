﻿using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunProject.Client.Helpers
{
    public abstract class UserData
    {
        public static User CurrentUser { get; set; }
    }
}
