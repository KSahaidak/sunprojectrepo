﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.EntityFrameworkCore;
using SunProject.Api.Controllers;
using SunProject.Api.DataContext;
using SunProject.Api.Services;
using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SunProject.Api.Tests.Controllers
{
    public class UsersControllerTests
    {
        private static readonly Fixture Fixture = new Fixture();
        IDataProcessService _data;
        public UsersControllerTests()
        {
            var ContextMock = new Mock<SunInfoDataContext>();
            IList<User> Users = GenerateUsers();
            ContextMock.Setup(x => x.Users).ReturnsDbSet(Users);
            _data = new DataProcess(ContextMock.Object);
        }

        [Fact]
        public async Task Create_ReturnsOkResultIfSuccessful()
        {
            // Arrange
            var controller = new UsersController(_data);
            User user = new User() { Id = 11, Login = "123456verynewlogin", Password = "Pass" };

            // Act
            IActionResult actionResult = await controller.Create(user);
            var okResult = actionResult as OkResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        [InlineData("admin")]
        public async Task Create_ReturnsConflictIfInvalidLogin(string login)
        {
            // Arrange
            var controller = new UsersController(_data);
            User user = new User() { Login = login };

            // Act
            IActionResult actionResult = await controller.Create(user);
            var conflict = actionResult as ConflictObjectResult;

            // Assert
            Assert.NotNull(conflict);
            Assert.Equal(409, conflict.StatusCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        public async Task Create_ReturnsConflictIfInvalidPassword(string password)
        {
            // Arrange
            var controller = new UsersController(_data);
            User user = new User() { Login = "SomeLogin", Password = password };

            // Act
            IActionResult actionResult = await controller.Create(user);
            var conflict = actionResult as ConflictObjectResult;

            // Assert
            Assert.NotNull(conflict);
            Assert.Equal(409, conflict.StatusCode);
        }

        [Theory]
        [MemberData(nameof(EditUserDataValid))]
        public async Task Edit_ReturnsOKIfValidParameters(string password, bool? access)
        {
            // Arrange
            var controller = new UsersController(_data);
            User user = new User() { Id = 1, Password = password, IsAdmin = access };

            // Act
            IActionResult actionResult = await controller.Edit(user);
            var ok = actionResult as OkResult;

            // Assert
            Assert.NotNull(ok);
            Assert.Equal(200, ok.StatusCode);
        }

        [Theory]
        [MemberData(nameof(EditUserDataInvalid))]
        public async Task Edit_ReturnsConflictIfInvalidParameters(string password, bool? access)
        {
            // Arrange
            var controller = new UsersController(_data);
            User user = new User() { Id = 1, Password = password, IsAdmin = access };

            // Act
            IActionResult actionResult = await controller.Edit(user);
            var conflict = actionResult as ConflictObjectResult;

            // Assert
            Assert.NotNull(conflict);
            Assert.Equal(409, conflict.StatusCode);
        }

        // Helpers
        public static IEnumerable<object[]> EditUserDataValid()
        {
            yield return new object[] { "pass", null };
            yield return new object[] { null, true };
            yield return new object[] { "pass", true };
        }
        public static IEnumerable<object[]> EditUserDataInvalid()
        {
            yield return new object[] { null, null };
            yield return new object[] { " ", null };
        }
        private static IList<User> GenerateUsers()
        {
            IList<User> users = new List<User>
            {
                Fixture.Build<User>()
                    .With(u => u.Id, 1)
                    .With(u => u.IsAdmin, true)
                    .With(u => u.Login, "admin")
                    .With(u => u.Password, "admin").Create(),
                Fixture.Build<User>()
                    .With(u => u.Id, 2)
                    .With(u => u.Login, "second").Create(),
                Fixture.Build<User>()
                    .With(u => u.Id, 3)
                    .With(u => u.Login, "third").Create()
            };
            return users;
        }
    }
}
