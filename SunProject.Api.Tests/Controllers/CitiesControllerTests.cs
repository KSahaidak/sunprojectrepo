﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.EntityFrameworkCore;
using SunProject.Api.Controllers;
using SunProject.Api.DataContext;
using SunProject.Api.Helpers;
using SunProject.Api.Services;
using SunProject.Shared.Helpers;
using SunProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SunProject.Api.Tests.Controllers
{
    public class CitiesControllerTests
    {
        private static readonly Fixture Fixture = new Fixture();
        IDataProcessService _data;
        IApiService<Point, string> apiservice;

        public CitiesControllerTests()
        {
            var ContextMock = new Mock<SunInfoDataContext>();
            IList<City> Cities = GenerateCities();
            ContextMock.Setup(x => x.Cities).ReturnsDbSet(Cities);
            _data = new DataProcess(ContextMock.Object);
            apiservice = new MapApiProcess(new ApiClientHelper());
        }

        [Fact]
        public async Task Create_ReturnsOkIfValid()
        {
            // Arrange
            var controller = new CitiesController(_data, apiservice);
            City city = new City() { Name = "Львов" };

            // Act
            IActionResult actionResult = await controller.Create(city);
            var okResult = actionResult as OkResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        [InlineData("Запорожье")]
        [InlineData("gibberish122334ljflkejfkmlkfgej")]
        public async Task Create_ReturnsConflictIfInvalidInput(string name)
        {
            // Arrange
            var controller = new CitiesController(_data, apiservice);
            City city = new City() { Name = name };

            // Act
            IActionResult actionResult = await controller.Create(city);
            var conflict = actionResult as ConflictObjectResult;

            // Assert
            Assert.NotNull(conflict);
            Assert.Equal(409, conflict.StatusCode);
        }



        // Helpers
        private static IList<City> GenerateCities()
        {
            IList<City> cities = new List<City>
            {
                new City(){ Id = 1, Name = "Запорожье", Latitude = 47.84645462036133 , Longitude = 35.149269104003906 },
                new City(){ Id = 2, Name = "Киев", Latitude = 50.447731018066406 , Longitude = 30.54271697998047 },
                new City(){ Id = 3, Name = "Одесса", Latitude = 46.47661209106445 , Longitude = 30.707307815551758 }
            };
            return cities;
        }
    }
}
