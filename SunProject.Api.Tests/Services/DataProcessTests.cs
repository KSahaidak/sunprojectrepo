﻿using SunProject.Api.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using SunProject.Api.DataContext;
using SunProject.Shared.Models;
using AutoFixture;
using Moq.EntityFrameworkCore;
using System.Linq;

namespace SunProject.Api.Tests.Services
{
    // TESTING BASIC ACTION OF USERS METHODS IN DATAACCESS
    public class DataProcessTests
    {
        private static readonly Fixture Fixture = new Fixture();
        private static Mock<SunInfoDataContext> ContextMock;

        public DataProcessTests()
        {
            ContextMock = new Mock<SunInfoDataContext>();
            IList<User> Users = GenerateUsers();
            ContextMock.Setup(x => x.Users).ReturnsDbSet(Users);
        }

        [Fact]
        public async Task FindUser_ById_ShouldReturnUserIfExist()
        {
            // Arrange
            var dataprocess = new DataProcess(ContextMock.Object);
            var expectedUser = Fixture.Build<User>().With(u => u.Id, 1).Create();

            // Act
            var actualUser = dataprocess.FindUser(1);

            // Assert
            Assert.Equal(expectedUser.Id, actualUser.Id);
        }
        [Fact]
        public async Task FindUser_ById_ShouldThrowExceptionIfUserNotExist()
        {
            // Arrange
            var dataprocess = new DataProcess(ContextMock.Object);

            // Assert
            await Assert.ThrowsAsync<ArgumentException>(() => dataprocess.FindUser(5));
        }
        [Fact]
        public async Task UserExist_ShouldReturnTrueIfLoginExist()
        {
            // Arrange
            var dataprocess = new DataProcess(ContextMock.Object);
            var expectedUser = Fixture.Build<User>()
                    .With(u => u.Login, "admin").Create();

            // Act
            bool exist = await dataprocess.UserExist("admin");

            // Assert
            Assert.True(exist);
        }
        [Fact]
        public async Task UserExist_ShouldReturnFalseIfLoginNotExist()
        {
            // Arrange
            var dataprocess = new DataProcess(ContextMock.Object);

            // Act
            bool exist = await dataprocess.UserExist("kate");

            // Assert
            Assert.False(exist);
        }
        [Fact]
        public async Task FindUser_Authentication_ShouldReturnUserIfCorrect()
        {
            // Arrange
            var dataprocess = new DataProcess(ContextMock.Object);

            // Act
            User actualUser = await dataprocess.FindUser("admin", "admin");

            // Assert
            Assert.NotNull(actualUser);
        }
        [Fact]
        public async Task FindUser_Authentication_ShouldThrowExceptionIfFails()
        {
            // Arrange
            var dataprocess = new DataProcess(ContextMock.Object);

            // Assert
            await Assert.ThrowsAsync<ArgumentException>(() => dataprocess.FindUser("admin", "123"));
        }
        [Theory]
        [MemberData(nameof(UserListData))]
        public async Task LoadUsers_ShouldReturnListOfUsers(User[] users, int count)
        {
            // Arrange
            IList<User> expectedList = new List<User>(users);
            ContextMock.Setup(x => x.Users).ReturnsDbSet(expectedList);
            var dataprocess = new DataProcess(ContextMock.Object);

            // Act
            List<User> actualList = await dataprocess.LoadUsersList();

            // Assert
            Assert.NotNull(actualList);
            Assert.Equal(count, actualList.Count());
        }

        // HELPERS
        public static IEnumerable<object[]> UserListData()
        {
            yield return new object[] { new User[] {  }, 0 };
            yield return new object[] { ContextMock.Object.Users.ToArray(), ContextMock.Object.Users.Count() };
        }

        private static IList<User> GenerateUsers()
        {
            IList<User> users = new List<User>
            {
                Fixture.Build<User>()
                    .With(u => u.Id, 1)
                    .With(u => u.IsAdmin, true)
                    .With(u => u.Login, "admin")
                    .With(u => u.Password, "admin").Create(),
                Fixture.Build<User>()
                    .With(u => u.Id, 2)
                    .With(u => u.Login, "second").Create(),
                Fixture.Build<User>()
                    .With(u => u.Id, 3)
                    .With(u => u.Login, "third").Create()
            };
            return users;
        }
    }
}
